package temp;

public class ConversaoDeTemperatura {
	public double CelsiusToFahrenheit(double c) {
		return (9*c/5)+32;
	}
	public double CelsiusToKelvin(double c) {
		return c + 273.15;
	}
	public double FahrenheitToCelsius(double f) {
		return 5*(f-32)/9;
	}
	public double KelvinToCelsius(double k) {
		return k - 273.15;
	}
	public double FahrenheitToKelvin(double f) {
		return 5*(f+459.67)/9;
	}
	public double KelvinToFahrenheit(double k) {
		return 9*(k-459.67);
	}
}
