package testes;
import temp.ConversaoDeTemperatura;
import java.util.Scanner;

public class Main {
	public static void main(String args[]) {
		ConversaoDeTemperatura cdt = new ConversaoDeTemperatura();
		Scanner sc = new Scanner(System.in);
		boolean loop = true;
		while(loop) {
			System.out.println("Qual temperatura deseja converter");
			System.out.println("1 - Celsiu para Fahrenheit");
			System.out.println("2 - Fahrenheit para Celsius");
			System.out.println("3 - Celsius para Kelvin");
			System.out.println("4 - Kelvin para Celsius");
			System.out.println("5 - Fahrenhiet para Kelvin");
			System.out.println("6 - Kelvin para Fahreinheit");
			System.out.println("Qualquer outro numero para sair");
			
			switch(sc.nextInt()) {
			case 1:
				System.out.println("Entre com a temperatura a converter:");
				System.out.printf("Temp: %.2f F\n", cdt.CelsiusToFahrenheit(sc.nextDouble()));
				break;
			case 2:
				System.out.println("Entre com a temperatura a converter:");
				System.out.printf("Temp: %.2f C\n", cdt.FahrenheitToCelsius(sc.nextDouble()));
				break;
			case 3:
				System.out.println("Entre com a temperatura a converter:");
				System.out.printf("Temp: %.2f K\n", cdt.CelsiusToKelvin(sc.nextDouble()));
				break;
			case 4:
				System.out.println("Entre com a temperatura a converter:");
				System.out.printf("Temp: %.2f C\n", cdt.KelvinToCelsius(sc.nextDouble()));
				break;
			case 5:
				System.out.println("Entre com a temperatura a converter:");
				System.out.printf("Temp: %.2f K\n", cdt.FahrenheitToKelvin(sc.nextDouble()));
				break;
			case 6:
				System.out.println("Entre com a temperatura a converter:");
				System.out.printf("Temp: %.2f F\n", cdt.FahrenheitToKelvin(sc.nextDouble()));
				break;
			default:
				System.out.println("Ate mais");
				loop = false;
				break;
			}
		}
	}
}
