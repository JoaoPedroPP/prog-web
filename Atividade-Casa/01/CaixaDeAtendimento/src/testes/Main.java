package testes;
import modelos.CaixaDeAtendimento;
import java.util.ArrayList;
import java.util.Random;

public class Main {

	public static void main(String[] args) {
		boolean loop = true;
		CaixaDeAtendimento caixa01 = new CaixaDeAtendimento(1);
		CaixaDeAtendimento caixa02 = new CaixaDeAtendimento(2);
		CaixaDeAtendimento caixa03 = new CaixaDeAtendimento(3);
		CaixaDeAtendimento caixa04 = new CaixaDeAtendimento(4);
		CaixaDeAtendimento caixa05 = new CaixaDeAtendimento(5);
		
		int senhaAtual = 0;
		
		while(loop) {
			double rand = Math.random()*100;
			if(rand <= 20) {
				if(!caixa01.isOcupado()) {
					caixa01.setSenha(++senhaAtual);
					caixa01.setOcupado(true);
					caixa01.chamaProximoFila();
					new Thread() {
						public void run() {
							try {
								double x = Math.random();
								x *= 10000;
								Thread.currentThread().sleep((long)x);
								caixa01.setOcupado(false);
							}
							catch(InterruptedException ex) {
								ex.printStackTrace();
							}
						}
					}.start();
				}
			}
			else if(rand > 20 && rand <= 40) {
				if(!caixa02.isOcupado()) {
					caixa02.setSenha(++senhaAtual);
					caixa02.setOcupado(true);
					caixa02.chamaProximoFila();
					new Thread() {
						public void run() {
							try {
								double x = Math.random();
								x *= 10000;
								Thread.currentThread().sleep((long)x);
								caixa02.setOcupado(false);
							}
							catch(InterruptedException ex) {
								ex.printStackTrace();
							}
						}
					}.start();
				}
			}
			else if(rand > 40 && rand <= 60) {
				if(!caixa03.isOcupado()) {
					caixa03.setSenha(++senhaAtual);
					caixa03.setOcupado(true);
					caixa03.chamaProximoFila();
					new Thread() {
						public void run() {
							try {
								double x = Math.random();
								x *= 10000;
								Thread.currentThread().sleep((long)x);
								caixa03.setOcupado(false);
							}
							catch(InterruptedException ex) {
								ex.printStackTrace();
							}
						}
					}.start();
				}
			}
			else if(rand > 60 && rand <= 80) {
				if(!caixa04.isOcupado()) {
					caixa04.setSenha(++senhaAtual);
					caixa04.setOcupado(true);
					caixa04.chamaProximoFila();
					new Thread() {
						public void run() {
							try {
								double x = Math.random();
								x *= 10000;
								Thread.currentThread().sleep((long)x);
								caixa04.setOcupado(false);
							}
							catch(InterruptedException ex) {
								ex.printStackTrace();
							}
						}
					}.start();
				}
			}
			else {
				if(!caixa05.isOcupado()) {
					caixa05.setSenha(++senhaAtual);
					caixa05.setOcupado(true);
					caixa05.chamaProximoFila();
					new Thread() {
						public void run() {
							double x = Math.random();
							x *= 10000;
							try {
								Thread.currentThread().sleep((long)x);
								caixa05.setOcupado(false);
							}
							catch(InterruptedException ex) {
								ex.printStackTrace();
							}
						}
					}.start();
				}
			}
		}
		
	}
}
