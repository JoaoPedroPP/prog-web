package modelos;

public class CaixaDeAtendimento {
	private int id;
	private int senha;
	private boolean ocupado;
	
	public CaixaDeAtendimento(int id) {
		this.id = id; 
	}
	
	public void chamaProximoFila() {
		System.out.printf("Senha: %d dirigir-se ao caixa: %d\n", this.senha, this.id);
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getSenha() {
		return senha;
	}
	public void setSenha(int senha) {
		this.senha = senha;
	}

	public boolean isOcupado() {
		return ocupado;
	}

	public void setOcupado(boolean ocupado) {
		this.ocupado = ocupado;
	}
}
