package bb.com.br.agenda.DAO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import bb.com.br.agenda.jdbc.ConnectionFactory;
import bb.com.br.agenda.modelo.Categoria;
import bb.com.br.agenda.modelo.Contato;

public class CategoriaDAO {
	private Connection connection;
	
	public void salvar(Categoria c) {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("sistemaagenda");
		EntityManager manager = factory.createEntityManager();
		try {
			manager.getTransaction().begin();
			manager.persist(c);
			manager.getTransaction().commit();
		} finally {
			if (manager.getTransaction().isActive())
				manager.getTransaction().rollback();
		}
		manager.close();
	}
	
	public Categoria localizaPorDescricao(String cat) {
		String sql = "SELECT * FROM Categoria WHERE Descricao=?";
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("sistemaagenda");
		EntityManager manager = factory.createEntityManager();
		Categoria cc = new Categoria();
		try {
			manager.getTransaction().begin();
			cc = (Categoria)manager.createQuery("SELECT cc FROM Categoria cc WHERE cc.descricao=:descricao").setParameter("descricao", cat).getSingleResult();
			manager.getTransaction().commit();
		} finally {
			if (manager.getTransaction().isActive())
				manager.getTransaction().rollback();
		}
		manager.close();
		return cc;
	}
}
