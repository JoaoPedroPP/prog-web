package bb.com.br.agenda.DAO;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class GeradorTabelas {
	public static void main(String[] args) {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("sistemaagenda");
		System.out.println("Tabela gerada");
		factory.close();
	}
}
