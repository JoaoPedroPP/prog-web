package bb.com.br.agenda.DAO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import bb.com.br.agenda.jdbc.ConnectionFactory;
import bb.com.br.agenda.modelo.Categoria;
import bb.com.br.agenda.modelo.Contato;

public class ContatoDAO {
	
	public void salvar(Contato c) {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("sistemaagenda");
		EntityManager manager = factory.createEntityManager();
		try {
			manager.getTransaction().begin();
			manager.merge(c);
			manager.getTransaction().commit();
		} finally {
			if (manager.getTransaction().isActive())
				manager.getTransaction().rollback();
		}
		manager.close();
	}
	
	public void removeContato(Contato cc) {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("sistemaagenda");
		EntityManager manager = factory.createEntityManager();
		cc = manager.find(Contato.class, cc.getId());
		try {
			manager.getTransaction().begin();
			manager.remove(cc);
			manager.getTransaction().commit();
		} finally {
			if (manager.getTransaction().isActive())
				manager.getTransaction().rollback();
		}
		manager.close();
	}
	
	public void updateContato(Contato c) {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("sistemaagenda");
		EntityManager manager = factory.createEntityManager();
		try {
			manager.getTransaction().begin();
			manager.merge(c);
			manager.getTransaction().commit();
		} finally {
			if (manager.getTransaction().isActive())
				manager.getTransaction().rollback();
		}
		manager.close();
	}
	
	public Contato searchName(Contato c) {
		String sql = "SELECT * FROM Contatos WHERE nome=?";
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("sistemaagenda");
		EntityManager manager = factory.createEntityManager();
		Contato cc = new Contato();
		try {
			manager.getTransaction().begin();
			cc = (Contato)manager.createQuery("SELECT cc FROM contatos cc WHERE cc.nome=:nome").setParameter("nome", c.getNome()).getSingleResult();
			manager.getTransaction().commit();
		} finally {
			if (manager.getTransaction().isActive())
				manager.getTransaction().rollback();
		}
		manager.close();
		return cc;
	}
	
	public List<Contato> listContato() {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("sistemaagenda");
		EntityManager manager = factory.createEntityManager();
		@SuppressWarnings("unchecked")
		List<Contato> contato = manager.createQuery("select c from contato c").getResultList();
		manager.close();
		return contato;
	}
}
