CREATE DATABASE agendadb;
CREATE TABLE Contatos(
	id serial NOT NULL,
	nome character varying(255) NOT NULL,
	email character varying(255) NOT NULL,
	endereco character varying(255) NOT NULL,
	categoria character varying(255),
	PRIMARY KEY(id)
);
CREATE TABLE Categoria(
	id serial NOT NULL,
	descricao character varying(255) NOT NULL
);