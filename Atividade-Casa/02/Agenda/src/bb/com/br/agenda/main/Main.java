package bb.com.br.agenda.main;

import bb.com.br.agenda.DAO.CategoriaDAO;
import bb.com.br.agenda.DAO.ContatoDAO;
import bb.com.br.agenda.modelo.Categoria;
import bb.com.br.agenda.modelo.Contato;

public class Main {
	public static void main(String[] args) {
		Contato contato = new Contato();
		Categoria categoria = new Categoria();
		ContatoDAO contatoDao = new ContatoDAO();
		CategoriaDAO categoriaDao = new CategoriaDAO();
		contato.setId(1);
		contato.setNome("joao pedro");
		contato.setEmail("poloni.ponce@aluno.ufabc.edu.br");
		contato.setEndereco("Av. do Estado");
		
//		cat.setId(1);
//		cat.setDescricao("TI");
		
//		contatoDao.salvar(c);//Funciona
//		contatoDao.removeContato(c);//Funciona
//		contatoDao.updateContato(c);//Funciona
//		Contato cc = contatoDao.searchName(c);//Funciona
//		System.out.println(cc.getAll());
		
		//Salvar a categoria
		categoria.setDescricao("TI");
		categoriaDao.salvar(categoria);//Funciona
		
		//buscar categoria por descricao
		categoria = categoriaDao.localizaPorDescricao("TI");
		
		//atribuir ctegoria a contato
		contato.setCategoria(categoria);
		
		//Salvar o contato
		contatoDao.salvar(contato);
	}
}
