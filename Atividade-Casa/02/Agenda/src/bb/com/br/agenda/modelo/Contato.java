package bb.com.br.agenda.modelo;

public class Contato {
	private long id;
	private String nome;
	private String email;
	private String endereco;
	private Categoria categoria;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getEndereco() {
		return endereco;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public Categoria getCategoria() {
		return categoria;
	}
	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}
	public String getAll() {
		return "Id: "+this.id+", Nome: "+this.nome+", Email: "+this.email+", Endereco: "+this.endereco;
	}
}
