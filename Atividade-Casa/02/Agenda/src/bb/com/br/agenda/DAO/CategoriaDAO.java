package bb.com.br.agenda.DAO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import bb.com.br.agenda.jdbc.ConnectionFactory;
import bb.com.br.agenda.modelo.Categoria;

public class CategoriaDAO {
	private Connection connection;
	public CategoriaDAO() {
		this.connection = new ConnectionFactory().getConnection();
	}
	
	public void salvar(Categoria c) {
		String sql = "INSERT INTO Categoria(descricao) VALUES(?)";
		try {
			PreparedStatement stmt = connection.prepareStatement(sql);
			stmt.setString(1, c.getDescricao());
			stmt.execute();
			stmt.close();
		} catch(SQLException e) {
			throw new RuntimeException(e);
		}
	}
	
	public Categoria localizaPorDescricao(String cat) {
		String sql = "SELECT * FROM Categoria WHERE Descricao=?";
		Categoria c = new Categoria();
		try {
			PreparedStatement stmt = connection.prepareStatement(sql);
			stmt.setString(1, cat);
			ResultSet rs = stmt.executeQuery();
			if(rs.next()) {
				c.setId(rs.getLong("id"));
				c.setDescricao(rs.getString("descricao"));
				System.out.println(c.getAll());
			}
			rs.close();
			stmt.close();
		} catch(SQLException e) {
			throw new RuntimeException(e);
		}
		return c;
	}
}
