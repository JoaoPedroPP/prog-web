package bb.com.br.agenda.modelo;

public class Categoria {
	private long id;
	private String descricao;
	private String email;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getAll() {
		return "Id: "+this.id+", Descricao: "+this.descricao;
	}
}
