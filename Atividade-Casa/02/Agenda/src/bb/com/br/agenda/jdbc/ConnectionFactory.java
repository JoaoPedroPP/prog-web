package bb.com.br.agenda.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;

public class ConnectionFactory {
	public Connection getConnection() {
		System.out.println("Conectado ao banco de dados");
		try {
			String url = "jdbc:postgresql://localhost/agendadb";
			return DriverManager.getConnection(url, "postgres", "joaopedro");
		} catch(Exception e) {
			throw new RuntimeException(e);
		}
	}
}
