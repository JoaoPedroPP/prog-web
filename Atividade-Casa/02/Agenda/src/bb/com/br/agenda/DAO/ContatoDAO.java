package bb.com.br.agenda.DAO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import bb.com.br.agenda.jdbc.ConnectionFactory;
import bb.com.br.agenda.modelo.Categoria;
import bb.com.br.agenda.modelo.Contato;

public class ContatoDAO {
	private Connection connection;
	public ContatoDAO() {
		this.connection = new ConnectionFactory().getConnection();
	}
	
	public void salvar(Contato c) {
		String sql = "INSERT INTO Contatos (id, nome, email, endereco, categoria) VALUES (?, ?, ?, ?, ?)";
		try {
			PreparedStatement stmt = connection.prepareStatement(sql);
			stmt.setLong(1, c.getId());
			stmt.setString(2, c.getNome());
			stmt.setString(3, c.getEmail());
			stmt.setString(4, c.getEndereco());
			Categoria cat = new Categoria();
			cat = c.getCategoria();
			stmt.setString(5, cat.getDescricao());
			stmt.execute();
			stmt.close();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
	
	public void removeContato(Contato c) {
		String sql = "DELETE FROM Contatos WHERE id=?";
		try {
			PreparedStatement stmt = connection.prepareStatement(sql);
			stmt.setLong(1, c.getId());
			stmt.execute();
			stmt.close();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
	
	public void updateContato(Contato c) {
		String sql = "UPDATE Contatos SET nome=?, email=?, endereco=?, categoria=? WHERE id=?";
		try {
			PreparedStatement stmt = connection.prepareStatement(sql);
			stmt.setString(1, c.getNome());
			stmt.setString(2, c.getEmail());
			stmt.setString(3, c.getEndereco());
			stmt.setString(4, c.getCategoria().getDescricao());
			stmt.setLong(5, c.getId());
			stmt.execute();
			stmt.close();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
	
	public Contato searchName(Contato c) {
		String sql = "SELECT * FROM Contatos WHERE nome=?";
		Contato cc = new Contato();
		try {
			PreparedStatement stmt = connection.prepareStatement(sql);
			stmt.setString(1, c.getNome());
			ResultSet rs = stmt.executeQuery();
			if(rs.next()) {
				cc.setId(rs.getLong("id"));
				cc.setNome(rs.getString("nome"));
				cc.setEmail(rs.getString("email"));
				cc.setEndereco(rs.getString("endereco"));
				Categoria cat = new Categoria();
				cat.setDescricao(rs.getString("categoria"));
				cc.setCategoria(cat);
			}
			rs.close();
			stmt.close();
		} catch(SQLException e) {
			throw new RuntimeException(e);
		}
		return cc;
	}
	
	public ArrayList<Contato> listContato() {
		ArrayList<Contato> css = new ArrayList<Contato>();
		String sql = "SELECT * FROM Contatos";
		try {
			PreparedStatement stmt = connection.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery();
			while(rs.next()) {
				Contato c = new Contato();
				c.setId(rs.getLong("id"));
				c.setNome(rs.getString("nome"));
				c.setEmail(rs.getString("email"));
				c.setEndereco(rs.getString("endereco"));
				Categoria cat = new Categoria();
				cat.setDescricao(rs.getString("categoria"));
				c.setCategoria(cat);
				css.add(c);
			}
			rs.close();
			stmt.close();
		} catch(SQLException e) {
			throw new RuntimeException(e);
		}
		return css;
	}
}
