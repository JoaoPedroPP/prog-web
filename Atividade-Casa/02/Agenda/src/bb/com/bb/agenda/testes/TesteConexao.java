package bb.com.bb.agenda.testes;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class TesteConexao {
	public static void main(String[] args) {
		Connection conexao = null;
		try {
			String url = "jdbc:postgresql://localhost/agendadb";
			conexao = DriverManager.getConnection(url, "postgres", "joaopedro");
			System.out.println("Conectou");
			} catch(SQLException e1) {
				System.out.println("Erro na conexao "+ e1.getMessage());
		} finally {
			try {
				conexao.close();
			} catch(SQLException e2) {
				System.out.println("Erro ao fechar "+e2.getMessage());
			}
		}
	}
}
