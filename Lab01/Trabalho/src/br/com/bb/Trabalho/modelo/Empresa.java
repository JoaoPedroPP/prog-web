package br.com.bb.Trabalho.modelo;
import br.com.bb.Trabalho.modelo.Funcionario;
import java.util.ArrayList;

public class Empresa {
	private String nome;
	private String cnpj;
	private ArrayList<Funcionario> prole = new ArrayList<Funcionario>();
	
	public Empresa(String nome, String cnpj) {
		this.nome = nome;
		this.cnpj = cnpj;
	}
	
	public void putProle(Funcionario x) {
		this.prole.add(x);
	}
	
	public ArrayList<Funcionario> getProle(){
		return this.prole;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
}
