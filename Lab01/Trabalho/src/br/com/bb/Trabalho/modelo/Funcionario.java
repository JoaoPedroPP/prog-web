package br.com.bb.Trabalho.modelo;

public class Funcionario {
	private String nome;
	private String cpf;
	private boolean ativo;
	private String setor;
	private double salario;
	
	public Funcionario(String nome, String cpf, boolean ativo, String setor, double salario) {
		this.nome = nome;
		this.cpf = cpf;
		this.ativo = ativo;
		this.setor = setor;
		this.salario = salario;
	}
	
	public double ganhoAnual() {
		return this.salario*12;
	}
	
	public boolean isAtivo() {
		return ativo;
	}
	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getSetor() {
		return setor;
	}
	public void setSetor(String setor) {
		this.setor = setor;
	}
	public double getSalario() {
		return salario;
	}
	public void setSalario(double salario) {
		this.salario = salario;
	}
}