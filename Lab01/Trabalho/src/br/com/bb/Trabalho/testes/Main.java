package br.com.bb.Trabalho.testes;
import br.com.bb.Trabalho.modelo.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Empresa A = new Empresa("A ltd", "123456789");
		Scanner sc = new Scanner(System.in);
		boolean loop = true;
		
		A.putProle(new Funcionario("Joao", "12345", true, "Executivo", 15000));
		A.putProle(new Funcionario("Pedro", "23451", true, "Desenvolvimento", 8000));
		A.putProle(new Funcionario("George", "34512", false, "Engenharia", 10000));
		A.putProle(new Funcionario("Joaquim", "45123", false, "Engenharia", 10000));
		A.putProle(new Funcionario("Pompeu", "51234", true, "Tunelaria", 5000));
		while(loop) {
			System.out.println("Opcoes");
			System.out.println("1 - Listar todos os funcionarios");
			System.out.println("2 - Listar funcionarios por setor");
			System.out.println("3 - Listar funcionarios ativos");
			System.out.println("4 - Encontrar um funcionario por cpf");
			System.out.println("Digite qualquer outro numero para sair");
			
			switch (sc.nextInt()) {
			case 1:
				listaFuncionarioNomeCpf(A);
				break;
			case 2:
				System.out.println("Qual setor gostaria de pesquisar ?");
				System.out.println("Executivo, Desenvolvimento, Engenharia ou Tunelaria ?");
				listaFuncionarioSetor(A, sc.next());
				break;
			case 3:
				listaFuncionarioAtivo(A);
				break;
			case 4:
				System.out.println("Entre com o cpf que deseja procurar:");
				listaFuncionarioCpf(A, sc.next());
				break;
			default:
				loop = false;
				System.out.println("Ate mais");
				break;
			}
		}
	}
	
	public static Iterator<Funcionario> getIterator(Empresa x) {
		ArrayList<Funcionario> XF = x.getProle();
		Iterator<Funcionario> it = XF.iterator();
		return it;
	}
	
	public static void listaFuncionarioNomeCpf(Empresa X) {
		Iterator<Funcionario> it = getIterator(X);
		while(it.hasNext()) {
			Funcionario f = it.next();
			System.out.println("Nome: " + f.getNome() + ", cpf: " + f.getCpf());
		}
	}
	
	public static void listaFuncionarioSetor(Empresa X, String setor) {
		Iterator<Funcionario> it = getIterator(X);
		System.out.println("Os seguintes funcionarios trabalham no setor: " + setor);
		while(it.hasNext()) {
			Funcionario f = it.next();
			if(f.getSetor().equals(setor)) {
				System.out.println("Nome: " + f.getNome() + ", cpf: " + f.getCpf());
			}
		}
	}
	
	public static void listaFuncionarioAtivo(Empresa X) {
		Iterator<Funcionario> it = getIterator(X);
		System.out.println("Os seguintes funcionarios estao ativos:");
		while(it.hasNext()) {
			Funcionario f = it.next();
			if(f.isAtivo()) {
				System.out.println("Nome: " + f.getNome() + ", cpf: " + f.getCpf());
			}
		}
	}
	
	public static void listaFuncionarioCpf(Empresa X, String cpf) {
		Iterator<Funcionario> it = getIterator(X);
		System.out.println("Encontrei o seguinte funcionario:");
		while(it.hasNext()) {
			Funcionario f = it.next();
			if(f.getCpf().equals(cpf)) {
				System.out.println("Nome: " + f.getNome() + ", cpf: " + f.getCpf());
				return;
			}
		}
		System.out.println("cpf nao encontrado");
		return;
	}

}
