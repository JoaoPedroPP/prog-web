package br.com.bb.contacorrente.testes;
import br.com.bb.contacorrente.modelo.ContaCorrente;

import java.util.ArrayList;
import java.util.Scanner;

public class CriaContaCorrente {
	public static void main(String[] args) {
		ContaCorrente[] cc = new ContaCorrente[10];
		Scanner sc = new Scanner(System.in);
		int i = 0;
		for(i = 0; i < 1; i++) {
			cc[i] = new ContaCorrente();
			System.out.println("Entre com o numero da conta");
			cc[i].setNumero(sc.next());
			System.out.println("Entre com o numero da Agencia");
			cc[i].setAgencia(sc.next());
			System.out.println("Entre com uma breve descricao");
			cc[i].setDescricao(sc.next());
			System.out.println("Ativa(true) ou nao(false"); 
			cc[i].setAtiva(sc.nextBoolean());
			System.out.println("Variacao");
			cc[i].setVariacao(sc.nextInt());
			System.out.println("Id");
			cc[i].setId((long)sc.nextLong());
		}
		
		for(i = 0; i < 1; i++) {
			System.out.println("Conta Numero: "+ cc[i].getNumero() + ", Agencia: " + cc[i].getAgencia() + ", Descricao: " + cc[i].getDescricao() + ", Ativa: " + cc[i].isAtiva() + ", Variacao: "+ cc[i].getVariacao() + ", Id: "+ cc[i].getId());
		}
		
	}
}
