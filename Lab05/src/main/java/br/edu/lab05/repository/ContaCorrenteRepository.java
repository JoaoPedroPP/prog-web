package br.edu.lab05.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.edu.lab05.model.ContaCorrente;

@Repository
public interface ContaCorrenteRepository extends JpaRepository<ContaCorrente, Long> {
	
	@Query(nativeQuery=false, value="SELECT a FROM ContaCorrente a WHERE a.id=:id")
	List<ContaCorrente> findUser(@Param("id") Long id);
	
	@Transactional
	@Modifying
	@Query(nativeQuery=false, value="UPDATE ContaCorrente a SET a.numero=:numero, a.agencia=:agencia, a.descricao=:descricao, a.ativa=:ativa, a.variacao=:variacao WHERE a.id=:id")
	void updateTable(@Param("id") Long id, @Param("numero") String numero, @Param("agencia") String agencia, @Param("descricao") String descricao, @Param("variacao") int variacao, @Param("ativa") boolean ativa);
//	public List<ContaCorrenteDAO> getAll();

	@Query(nativeQuery=false, value="SELECT a FROM ContaCorrente a WHERE a.id=:id")
	ContaCorrente find1User(@Param("id") Long id);
}
