package br.edu.lab05.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.edu.lab05.model.Banco;

public interface BancoRepository extends JpaRepository<Banco, Long> {
	@Query(nativeQuery=false, value="SELECT a FROM Banco a WHERE a.id=:id")
	List<Banco> findUser(@Param("id") Long id);
}
