package br.edu.lab05.model;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.edu.lab05.repository.BancoRepository;

@Service
public class BancoDAO {
	@Autowired
	BancoRepository repository;
	public void save(Banco bb){
		repository.save(bb);
	}
	
	public List<Banco> find(Long id){
		return repository.findUser(id);
	}
	
	public void remove(Banco bb){
		repository.delete(bb);
	}
}
