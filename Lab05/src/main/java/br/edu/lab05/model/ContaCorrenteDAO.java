package br.edu.lab05.model;


import java.util.List;
import java.util.ArrayList;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import br.edu.lab05.model.*;
import br.edu.lab05.repository.ContaCorrenteRepository;

@Service
public class ContaCorrenteDAO {
	@Autowired
	ContaCorrenteRepository repository;
	public void save(ContaCorrente cc) {
		repository.save(cc);
	}
	
	public List<ContaCorrente> find(Long id) {
		return repository.findUser(id);
	}
	
	public void update(ContaCorrente cc) {
		System.out.println(cc.getId());
		repository.save(cc);
	}
	
	public void remove(ContaCorrente cc){
		repository.deleteById(cc.getId());
	}
}
