package br.edu.lab05.random;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class TableGenerator {
	public static void main(String[] args) {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("sistemabancario");
		System.out.println("Tabela gerada!");
		factory.close();
	}
}
