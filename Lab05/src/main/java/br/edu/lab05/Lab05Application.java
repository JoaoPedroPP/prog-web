package br.edu.lab05;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import br.edu.lab05.random.TableGenerator;

@SpringBootApplication
public class Lab05Application {

	public static void main(String[] args) {
		TableGenerator tb = new TableGenerator();
		SpringApplication.run(Lab05Application.class, args);
	}
}
