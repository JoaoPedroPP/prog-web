package br.edu.lab05.controller;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import br.edu.lab05.model.Banco;
import br.edu.lab05.model.BancoDAO;
import br.edu.lab05.repository.BancoRepository;

@Controller
public class BancoController {
	@Autowired
	BancoDAO bbDAO;
	@Autowired
	BancoRepository bb;
	
	@RequestMapping(value={"/banco"})
	@ResponseBody
	public ModelAndView index(){
		ModelAndView mv = new ModelAndView("indexb");
		mv.addObject("ccs", bb.findAll());
		return mv;
	}
	
	@RequestMapping("/homeb")
	public ModelAndView home() {
		return new ModelAndView("home");
	}
	
	@RequestMapping("/cadastrob")
	public ModelAndView cadastro() {
		return new ModelAndView("cadastro");
	}
	
	@RequestMapping("/api/newcadastrob")
	public RedirectView newCadastro(
			@RequestParam Long id,
			@RequestParam String nome,
			@RequestParam String codigo,
			@RequestParam String descricao,
			@RequestParam String endereco,
			Model model
			) {
		Banco cc = new Banco();
		if(id != null){
			cc.setId(id);
		}
		cc.setNome(nome);
		cc.setCodigo(codigo);
		cc.setDescricao(descricao);
		cc.setEndereco(endereco);
		
		bbDAO.save(cc);
		
		return new RedirectView("/banco");
	}
	
	@RequestMapping("/editb/{id}")
	public ModelAndView editar(
			@PathVariable Long id,
			Model model
			){
		System.out.println(id);
		List<Banco> c = new ArrayList<Banco>();
		c = bb.findUser(id);
		ModelAndView mv = new ModelAndView("editb");
		if(c.size()>0) {
			mv.addObject("id", c.get(0).getId());
			mv.addObject("numero", c.get(0).getNome());
			mv.addObject("agencia", c.get(0).getCodigo());
			mv.addObject("descricao", c.get(0).getDescricao());
			mv.addObject("ativa", c.get(0).getEndereco());
		}
		return mv;
	}
	
	@RequestMapping("/apib/edit")
	public RedirectView update(
			@RequestParam Long id,
			@RequestParam String nome,
			@RequestParam String codigo,
			@RequestParam String descricao,
			@RequestParam String endereco,
			Model model
			) {
		Banco cd = new Banco();
		cd.setNome(nome);
		cd.setCodigo(codigo);
		cd.setDescricao(descricao);
		cd.setEndereco(endereco);
		cd.setId(id);
		bbDAO.save(cd);
		return new RedirectView("/");
	}
	
	@RequestMapping("/api/removeb")
	public RedirectView removeUser(
			@RequestParam Long id,
			@RequestParam String nome,
			@RequestParam String codigo,
			@RequestParam String descricao,
			@RequestParam String endereco,
			Model model
			) {
		Banco ce = new Banco();
		ce.setNome(nome);
		ce.setCodigo(codigo);
		ce.setDescricao(descricao);
		ce.setEndereco(endereco);
		ce.setId(id);
		bbDAO.remove(ce);
		return new RedirectView("/banco");
	}
}
