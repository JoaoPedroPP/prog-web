package br.edu.lab05.controller;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import br.edu.lab05.model.Banco;
import br.edu.lab05.model.ContaCorrente;
import br.edu.lab05.model.ContaCorrenteDAO;
import br.edu.lab05.repository.BancoRepository;
import br.edu.lab05.repository.ContaCorrenteRepository;

@Controller
public class ContaCorrenteController {
	
	@Autowired
	ContaCorrenteDAO ccDAO;
	@Autowired
	ContaCorrenteRepository cc;
	@Autowired
	BancoRepository bb;
	
	@RequestMapping(value={"/", "/index"})
	@ResponseBody
	public ModelAndView index(){
		ModelAndView mv = new ModelAndView("index");
		mv.addObject("ccs", cc.findAll());
		mv.addObject("bbs", bb.findAll());
		return mv;
	}
	
	@RequestMapping("/home")
	public ModelAndView home() {
		return new ModelAndView("home");
	}
	
	@RequestMapping("/cadastro")
	public ModelAndView cadastro() {
		return new ModelAndView("cadastro");
	}
	
	@RequestMapping("/api/newcadastro")
	public RedirectView newCadastro(
			@RequestParam Long id,
			@RequestParam String numero,
			@RequestParam String agencia,
			@RequestParam String descricao,
			@RequestParam String banco,
			@RequestParam boolean ativa,
			Model model
			) {
		ContaCorrente cc = new ContaCorrente();
		Banco bb = new Banco();
		if(id != null){
			cc.setId(id);
		}
		cc.setNumero(numero);
		cc.setAgencia(agencia);
		cc.setDescricao(descricao);
		cc.setAtiva(ativa);
		cc.setVariacao(0);
		bb.setNome(banco);
		cc.setBanco(bb);
		
		ccDAO.save(cc);
		
		return new RedirectView("/");
	}
	
	@RequestMapping("/edit/{id}")
	public ModelAndView editar(
			@PathVariable Long id,
			Model model
			){
		System.out.println(id);
		List<ContaCorrente> c = new ArrayList<ContaCorrente>();
		c = cc.findUser(id);
		ModelAndView mv = new ModelAndView("edit");
		if(c.size()>0) {
			mv.addObject("id", c.get(0).getId());
			mv.addObject("numero", c.get(0).getNumero());
			mv.addObject("agencia", c.get(0).getAgencia());
			mv.addObject("descricao", c.get(0).getDescricao());
			mv.addObject("ativa", c.get(0).isAtiva());
			mv.addObject("variacao", c.get(0).getVariacao());
		}
		return mv;
	}
	
	@RequestMapping("/api/edit")
	public RedirectView update(
			@RequestParam Long id,
			@RequestParam String numero,
			@RequestParam String agencia,
			@RequestParam String descricao,
			@RequestParam double variacao,
			@RequestParam boolean ativa,
			Model model
			) {
		ContaCorrente cd = new ContaCorrente();
		cd.setNumero(numero);
		cd.setAgencia(agencia);
		cd.setDescricao(descricao);
		cd.setAtiva(ativa);
		cd.setVariacao((int)variacao);
		cd.setId(id);
		ccDAO.update(cd);
		return new RedirectView("/");
	}
	
	@RequestMapping("/api/remove")
	public RedirectView removeUser(
			@RequestParam Long id,
			@RequestParam String numero,
			@RequestParam String agencia,
			@RequestParam String descricao,
			@RequestParam boolean ativa,
			Model model
			) {
		ContaCorrente ce = new ContaCorrente();
		ce.setNumero(numero);
		ce.setAgencia(agencia);
		ce.setDescricao(descricao);
		ce.setAtiva(true);
		ce.setId(id);
		ccDAO.remove(ce);
		return new RedirectView("/");
	}
}
