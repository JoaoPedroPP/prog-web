<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html lang="pt-BR">
<head>
<meta charset="UTF-8" />
<title>Lista de Contas Corrente</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4"
    crossorigin="anonymous">
<link rel="stylesheet" href="../css/">
</head>
<body>
	<script type="application/javascript">
			function copiaDados(id, opcao){
				document.getElementById('id').value = id;
				var tableRow = document.getElementById('row_'+id);
				var cells = tableRow.getElementsByTagName("td");
				document.getElementById('numero').value = cells[0].textContent;
				document.getElementById('agencia').value = cells[1].textContent;
				document.getElementById('descricao').value = cells[2].textContent;
				getBanco();
				if(opcao == 'remover'){
					document.getElementById("alterar_"+id.toString()).checked = false;
					document.getElementById("deletar_"+id.toString()).checked = true;
					document.getElementById("button").innerHTML = "Remover";
					document.getElementById("ativa").checked = true;
					document.getElementById("ativa").style.visibility = "hidden";
					document.getElementById("idL").style.visibility = "hidden";
					document.getElementById("formc").action = "/api/remove";
				}
				if(opcao == 'salvar'){
					document.getElementById("deletar_"+id.toString()).checked = false;
					document.getElementById("alterar_"+id.toString()).checked = true;
					document.getElementById("button").innerHTML = "Salvar";
					document.getElementById("ativa").checked = false;
					document.getElementById("ativa").style.visibility = "visible";
					document.getElementById("idL").style.visibility = "visible";
					document.getElementById("formc").action = "/api/newcadastro";
				}
			}
			function remove(){
				if(document.getElementById("button").innerHTML == "Salvar"){
					return true
				}
				if(document.getElementById("button").innerHTML == "Remover"){
					return confirm("Confirma a remocacao da conta "+ document.getElementById('numero').value + ", agencia " + document.getElementById('agencia').value + " ?");
				}
			}
			function getBanco(){
				
			}
	</script>
	<table border="1">
		<thead>
			<tr>
				<th><b>Numero</b></th>
				<th><b>Agencia</b></th>
				<th><b>Descricao</b></th>
				<th><b>Editar</b></th>
				<th><b>Remover</b></th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${ccs}" var="cc">
				<tr id="row_${cc.id}">
					<td>${cc.numero}</td>
					<td>${cc.agencia}</td>
					<td>${cc.descricao}"</td>
					<td align="center"><input id="alterar_${cc.id}" type="radio" name="alterar" value="${cc.id}" onClick="copiaDados(value, 'salvar')"></td>
					<td align="center"><input id="deletar_${cc.id}" type="radio" name="deletar" value="${cc.id}" onClick="copiaDados(value, 'remover')"></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	<br>
	<form id="formc" action="/api/newcadastro">
			<label for="id">ID:</label>
			<input type="text" name="id" id="id" class="dados" readonly><br>
			<label for="numero">Numero:</label>
			<input id="numero" class="dados" name="numero" id="numero" type="text"><br>
			<label for="agencia">Agencia:</label>
			<input id="agencia" class="dados" name="agencia" id="agencia" type="text"><br>
			<label for="descricao">Descricao:</label>
			<input id="descricao" class="dados" name="descricao" id="descricao" type="text"><br>
			<label for="banco">Banco</label>
			<select name="banco">
				<c:forEach items="${bbs}" var="bb">
					<option value="${bb.nome}">${bb.nome}</option>
				</c:forEach>
			</select><br>
			<label id="idL" for="ativa">Ativar conta?</label>
			<input id="ativa" class="dados" name="ativa" type="checkbox" value="True"><br>
			<button id="button" type="submit" onClick="return remove()">Salvar</button>
		</form>		
</body>
</html>