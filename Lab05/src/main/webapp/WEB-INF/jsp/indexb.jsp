<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html lang="pt-BR">
<head>
<meta charset="UTF-8" />
<title>Lista de Contas Corrente</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4"
    crossorigin="anonymous">
<link rel="stylesheet" href="../css/">
</head>
<body>
	<script type="application/javascript">
			function copiaDados(id, opcao){
				document.getElementById('id').value = id;
				var tableRow = document.getElementById('row_'+id);
				var cells = tableRow.getElementsByTagName("td");
				document.getElementById('numero').value = cells[0].textContent;
				document.getElementById('agencia').value = cells[1].textContent;
				document.getElementById('descricao').value = cells[2].textContent;
				document.getElementById('ativa').value = cells[3].textContent;
				if(opcao == 'remover'){
					document.getElementById("alterar_"+id.toString()).checked = false;
					document.getElementById("deletar_"+id.toString()).checked = true;
					document.getElementById("button").innerHTML = "Remover";
					document.getElementById("formc").action = "/api/removeb";
				}
				if(opcao == 'salvar'){
					document.getElementById("deletar_"+id.toString()).checked = false;
					document.getElementById("alterar_"+id.toString()).checked = true;
					document.getElementById("button").innerHTML = "Salvar";
					document.getElementById("formc").action = "/api/newcadastrob";
				}
			}
			function remove(){
				if(document.getElementById("button").innerHTML == "Salvar"){
					return true
				}
				if(document.getElementById("button").innerHTML == "Remover"){
					return confirm("Confirma a remocacao da conta "+ document.getElementById('numero').value + ", agencia " + document.getElementById('agencia').value + " ?");
				}
			}
	</script>
	<table border="1">
		<thead>
			<tr>
				<th><b>Nome</b></th>
				<th><b>Codigo</b></th>
				<th><b>Descricao</b></th>
				<th><b>Endereco</b></th>
				<th><b>Editar</b></th>
				<th><b>Remover</b></th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${ccs}" var="cc">
				<tr id="row_${cc.id}">
					<td>${cc.nome}</td>
					<td>${cc.codigo}</td>
					<td>${cc.descricao}"</td>
					<td>${cc.endereco}"</td>
					<td align="center"><input id="alterar_${cc.id}" type="radio" name="alterar" value="${cc.id}" onClick="copiaDados(value, 'salvar')"></td>
					<td align="center"><input id="deletar_${cc.id}" type="radio" name="deletar" value="${cc.id}" onClick="copiaDados(value, 'remover')"></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	<br>
	<form id="formc" action="/api/newcadastrob">
			<label for="id">ID:</label>
			<input type="text" name="id" id="id" class="dados" readonly><br>
			<label for="nome">Nome:</label>
			<input id="numero" class="dados" name="nome" id="numero" type="text"><br>
			<label for="codigo">Codigo:</label>
			<input id="agencia" class="dados" name="codigo" id="agencia" type="text"><br>
			<label for="descricao">Descricao:</label>
			<input id="descricao" class="dados" name="descricao" id="descricao" type="text"><br>
			<label id="idL" for="endereco">Endereco</label>
			<input id="ativa" class="dados" name="endereco" type="text"><br>
			<button id="button" type="submit">Salvar</button>
		</form>		
</body>
</html>