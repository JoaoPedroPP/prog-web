<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="utf-8">
		<title>Sistema Bancario</title>
	</head>
	<body>
		<form action="/api/edit">
			<input name="id" type="number" value="${id}" hidden>
			<input name="variacao" type="number" value="${variacao}" hidden>
			<label for="numero">Numero:</label>
			<input id="numero" class="dados" name="numero" type="text" value="${numero}"><br>
			<label for="agencia">Agencia:</label>
			<input id="agencia" class="dados" name="agencia" type="text" value="${agencia}"><br>
			<label for="descricao">Descricao:</label>
			<input id="descricao" class="dados" name="descricao" type="text" value="${descricao}"><br>
			<label for="ativa">Ativar conta?</label>
			<input id="ativa" class="dados" name="ativa" type="checkbox" value="True"><br>
			<button type="submit">Salvar</button>
		</form>
	</body>
</html>