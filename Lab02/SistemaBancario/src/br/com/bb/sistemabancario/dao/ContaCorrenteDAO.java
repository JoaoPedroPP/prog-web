package br.com.bb.sistemabancario.dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import br.com.bb.contacorrente.modelo.ContaCorrente;
import br.com.bb.sistemabancario.jdbc.ConnectionFactory;

public class ContaCorrenteDAO {
	private Connection connection;
	public ContaCorrenteDAO() {
		this.connection = new ConnectionFactory().getConnection();
	}
	
	public void insere(ContaCorrente cc) {
		String sql = "insert into contacorrente (numero, agencia, descricao, ativa, variacao) values (?,?,?,?,?)";
		try{
			PreparedStatement stmt = connection.prepareStatement(sql);
			stmt.setString(1, cc.getNumero());
			stmt.setString(2, cc.getAgencia());
			stmt.setString(3, cc.getDescricao());
			stmt.setBoolean(3, cc.isAtiva());
			stmt.setInt(5, cc.getVariacao());
			stmt.execute();
			stmt.close();
		} catch(SQLException e){
			throw new RuntimeException(e);
		}
	}
	
	public void altera(ContaCorrente cc) {
		String sql = "UPDATE ContaCorrente SET ativa=? WHERE='?'";
		try{
			PreparedStatement stmt = connection.prepareStatement(sql);
			stmt.setBoolean(1, cc.isAtiva());
			stmt.setLong(2, cc.getId());
			stmt.execute();
			stmt.close();
		} catch(SQLException e){
			throw new RuntimeException(e);
		}
	}
	
	public void remove(ContaCorrente cc) {
		String sql = "DELETE FROM ContaCorrente WHERE id=?";
		try{
			PreparedStatement stmt = connection.prepareStatement(sql);
			stmt.setLong(1, cc.getId());
			stmt.execute();
			stmt.close();
		} catch(SQLException e){
			throw new RuntimeException(e);
		}
	}
	
	public List<ContaCorrente> lista(){
		List<ContaCorrente> css = new ArrayList<ContaCorrente>();
		try{
			String sql = "SELECT * FROM ContaCorrente";
			PreparedStatement stmt = connection.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery();
			while(rs.next()){
				ContaCorrente cc = new ContaCorrente();
				cc.setId(rs.getLong("id"));
				cc.setNumero(rs.getString("numero"));
				css.add(cc);
			}
			rs.close();
			stmt.close();
		} catch(SQLException e){
			throw new RuntimeException(e);
		}
		return css;
	}
	
	
	
}

