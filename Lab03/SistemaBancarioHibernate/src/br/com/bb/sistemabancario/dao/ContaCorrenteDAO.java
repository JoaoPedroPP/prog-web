package br.com.bb.sistemabancario.dao;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.com.bb.sistemabancario.modelo.ContaCorrente;

public class ContaCorrenteDAO {
	public void save(ContaCorrente cc) {
			EntityManagerFactory factory = Persistence.createEntityManagerFactory("sistemabancario");
			EntityManager manager = factory.createEntityManager();
			ContaCorrente c = manager.find(ContaCorrente.class, cc.getId());
			try {
				manager.getTransaction().begin();
				manager.merge(cc);
				manager.getTransaction().commit();
			} finally {
				if (manager.getTransaction().isActive())
					manager.getTransaction().rollback();
			}
			manager.close();
		}
	
	public void altera(ContaCorrente cc) {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("sistemabancario");
		EntityManager manager = factory.createEntityManager();
		try {
			manager.getTransaction().begin();
			manager.merge(cc);
			manager.getTransaction().commit();
		} finally {
			if (manager.getTransaction().isActive())
				manager.getTransaction().rollback();
		}
		manager.close();
	}
	
	public void remove(ContaCorrente cc) {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("sistemabancario");
		EntityManager manager = factory.createEntityManager();
//		manager.getTransaction().begin();
		cc = manager.find(ContaCorrente.class, cc.getId());
		try {
			manager.getTransaction().begin();
			manager.remove(cc);
			manager.getTransaction().commit();
		} finally {
			if (manager.getTransaction().isActive())
				manager.getTransaction().rollback();
		}
		manager.close();
	}
	
	public List<ContaCorrente> lista(){
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("sistemabancario");
		EntityManager manager = factory.createEntityManager();
		@SuppressWarnings("unchecked")
		List<ContaCorrente> contas = manager.createQuery("select c from ContaCorrente c").getResultList();
		manager.close();
		return contas;
	}

	public ContaCorrente buscaPeloNumero(String numero){
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("sistemabancario");
		EntityManager manager = factory.createEntityManager();
		ContaCorrente cc = new ContaCorrente();
//		ContaCorrente cc = manager.find(ContaCorrente.class, numero);
		try {
			manager.getTransaction().begin();
			cc = (ContaCorrente)manager.createQuery("SELECT cc FROM ContaCorrente cc WHERE cc.numero=:numero").setParameter("numero", numero).getSingleResult();
			manager.getTransaction().commit();
		} finally {
			if (manager.getTransaction().isActive())
				manager.getTransaction().rollback();
		}
		manager.close();
		return cc;
	}
	
	public ContaCorrente buscaPorContaAgencia(String conta, String agencia){
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("sistemabancario");
		EntityManager manager = factory.createEntityManager();
		ContaCorrente cc = new ContaCorrente();
//		ContaCorrente cc = manager.find(ContaCorrente.class, numero);
		try {
			manager.getTransaction().begin();
			cc = (ContaCorrente)manager.createQuery("SELECT cc FROM ContaCorrente cc WHERE cc.numero=:conta AND cc.agencia=:agencia").setParameter("conta", conta).setParameter("agencia", agencia).getSingleResult();
			manager.getTransaction().commit();
		} finally {
			if (manager.getTransaction().isActive())
				manager.getTransaction().rollback();
		}
		manager.close();
		return cc;
	}
	
	public ContaCorrente buscaPorDescricao(String descricao){
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("sistemabancario");
		EntityManager manager = factory.createEntityManager();
		ContaCorrente cc = new ContaCorrente();
//		ContaCorrente cc = manager.find(ContaCorrente.class, numero);
		try {
			manager.getTransaction().begin();
			cc = (ContaCorrente)manager.createQuery("SELECT cc FROM ContaCorrente cc WHERE cc.descricao like :descricao").setParameter("descricao", descricao).getSingleResult();
			manager.getTransaction().commit();
		} finally {
			if (manager.getTransaction().isActive())
				manager.getTransaction().rollback();
		}
		manager.close();
		return cc;
	}
	
}

