package br.com.bb.contacorrente.testes;

import br.com.bb.sistemabancario.dao.ContaCorrenteDAO;
import br.com.bb.sistemabancario.modelo.ContaCorrente;

public class CriaContaCorrente {
	public static void main(String[] args) {
		boolean loop = true;
		Scanner sc = new Scanner(System.in);
		while (loop){
			System.out.println("1 - Insercao");
			System.out.println("2 - update");
			System.out.println("3 - remocao");
			System.out.println("4 - consultao");
			System.out.println("5 - sair");
			System.out.println("6 - Busca pelo numero");
			System.out.println("7 - Busca pelo conta e agencia");
			System.out.println("8 - Busca por descricao");
			
			switch (sc.nextInt()){
			case 1:
				ContaCorrente cc = new ContaCorrente();
				cc = new ContaCorrente();
				System.out.println("Entre com o numero da conta");
				cc.setNumero(sc.next());
				System.out.println("Entre com o numero da Agencia");
				cc.setAgencia(sc.next());
				System.out.println("Entre com uma breve descricao");
				cc.setDescricao(sc.next());
				System.out.println("Ativa(true) ou nao(false"); 
				cc.setAtiva(sc.nextBoolean());
				System.out.println("Variacao");
				cc.setVariacao(sc.nextInt());
				System.out.println("Id");
				cc.setId((long)sc.nextLong());
				
				ContaCorrenteDAO dao = new ContaCorrenteDAO();
				dao.save(cc);
				System.out.println("Gravado!");
				break;
			case 2:
				ContaCorrente ccU = new ContaCorrente();
				System.out.println("Entre com o numero da conta");
				ccU.setNumero(sc.next());
				System.out.println("Entre com o numero da Agencia");
				ccU.setAgencia(sc.next());
				System.out.println("Entre com uma breve descricao");
				ccU.setDescricao(sc.next());
				System.out.println("Ativa(true) ou nao(false"); 
				ccU.setAtiva(sc.nextBoolean());
				System.out.println("Variacao");
				ccU.setVariacao(sc.nextInt());
//				System.out.println("Id");
//				ccU.setId((long)sc.nextLong());
				
				ContaCorrenteDAO  daoU = new ContaCorrenteDAO();
				daoU.altera(ccU);
				System.out.println("Alterado");
				break;
			case 3:
				cc = new ContaCorrente();
				System.out.println("Entre com o id da conta");
				cc.setId(sc.nextLong());
				
				ContaCorrenteDAO daoR = new ContaCorrenteDAO();
				daoR.remove(cc);
				System.out.println("Removido");
				break;
			case 4:
				ContaCorrenteDAO daoL = new ContaCorrenteDAO();
				List<ContaCorrente> css = daoL.lista();
				for (ContaCorrente cc1:css){
					System.out.println("Conta Numero: "+ cc1.getNumero() + ", Agencia: " + cc1.getAgencia() + ", Descricao: " + cc1.getDescricao() + ", Ativa: " + cc1.isAtiva() + ", Variacao: "+ cc1.getVariacao() + ", Id: "+ cc1.getId());
				}
				break;
			case 5:
				loop = false;
				break;
			case 6:
				ContaCorrenteDAO daoHS = new ContaCorrenteDAO();
				ContaCorrente cchs = daoHS.buscaPeloNumero("444");
				System.out.println(cchs.getId());
				System.out.println("Buscado!");
				break;
			case 7:
				ContaCorrenteDAO daoHSS = new ContaCorrenteDAO();
				ContaCorrente cchss = daoHSS.buscaPorContaAgencia("444", "444");
				System.out.println(cchss.getId());
				System.out.println("Buscado!");
				break;
			case 8:
				ContaCorrenteDAO daoHSSS = new ContaCorrenteDAO();
				ContaCorrente cchsss = daoHSSS.buscaPorDescricao("444");
				System.out.println(cchsss.getId());
				System.out.println("Buscado!");
				break;
			default:
				System.out.println("Nao entendi");
			}
		}		
	}
}
